----
title: "Biostat 822 <br /> Spring 2018 <br /> Homework 2"
output:
  html_document:
    df_print: paged
---

<br />
<br />
<br />

1. Create a constructor function for an S3 object called <tt> fruit </tt> with the following fields:
   a. name
   b. average sugar-to-weight ratio
   c. average water content
```{r}
library(pryr)
fruit <- function(x,y = NA,z = NA){
  structure(list(name = x, average_sugar_to_weight_ratio = y, average_water_content = z), class ='fruit')
}
```

2. Create a method for the <tt> fruit </tt> object called <tt> is.citrus </tt> that returns TRUE if a fruit is citrus (orange, tangerine, grapefruit, lemon, lime).
```{r}
is.citrus <- function(x){
  UseMethod('is.citrus',x)
}
is.citrus.fruit <- function(x){
  ifelse(is.element(tolower(x$name),c('orange','tangerine','grapefruit','lemon','lime'))==TRUE,TRUE,FALSE)
}
```

3. Create the following instances of <tt> fruit </tt>:
   a. apple
   b. pineapple
   c. orange
   d. lemon
   e. grape
   
   Use these to test <tt> is.citrus </tt>
```{r}
apple <- fruit('apple')
is.citrus(apple)
pineapple <- fruit('pineapple')
is.citrus(pineapple)
orange <- fruit('orange')
is.citrus(orange)
lemon <- fruit('lemon')
is.citrus(lemon)
grape <- fruit('grape')
is.citrus(grape)
```

4. Repeat 1-3 using the S4 object system.
  1) Create a constructor function for an S3 object called <tt> fruit </tt> with the following fields:
   a. name
   b. average sugar-to-weight ratio
   c. average water content
```{r}
setClass('fruit',slots = list(name = 'character', average_sugar_to_weight_ratio = 'numeric', average_water_content = 'numeric'))
```
  2) Create a method for the <tt> fruit </tt> object called <tt> is.citrus </tt> that returns TRUE if a fruit is citrus (orange, tangerine, grapefruit, lemon, lime).
```{r}
setGeneric('is.citrus',function(x) standardGeneric('is.citrus'))
setMethod('is.citrus',signature = 'fruit',function(x){
  ifelse(is.element(tolower(x@name),c('orange','tangerine','grapefruit','lemon','lime'))==TRUE,TRUE,FALSE)
})
```
  3) Create the following instances of <tt> fruit </tt>:
   a. apple
   b. pineapple
   c. orange
   d. lemon
   e. grape
   
   Use these to test <tt> is.citrus </tt>
```{r}
apple <- new('fruit',name = 'apple')
is.citrus(apple)
pineapple <- new('fruit',name = 'pineapple')
is.citrus(pineapple)
orange <- new('fruit',name = 'orange')
is.citrus(orange)
lemon <- new('fruit',name = 'lemon')
is.citrus(lemon)
grape <- new('fruit',name = 'grape')
is.citrus(grape)
```
5. Complete exercises 2, 3 and 4 on page 140 of Advanced R.

Exercise 2:

Draw a diagram that shows the enclosing environments of this function:
```{r}
f1 <- function(x1){
  f2 <- function(x2){
    f3 <- function(x3){
      x1 + x2 + x3
    }
    f3(3)
  }
  f2(2)
}
f1(1)
```
```{r echo=FALSE, out.width='100%'}
knitr::include_graphics('./Screen_Shot1.png')
```
Exercise 3:
Expand your previous diagram to show function bindings.
```{r echo=FALSE, out.width='100%'}
knitr::include_graphics('./Screen_Shot2.png')
```
Exercise 4:
Expand it again to show the execution and calling environments.
```{r echo=FALSE, out.width='100%'}
knitr::include_graphics('./Screen_Shot3.png')
```